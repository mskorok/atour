ALTER TABLE tours_countries DROP FOREIGN KEY FK_7AA0413F15ED8D43;
ALTER TABLE tours_countries DROP FOREIGN KEY FK_7AA0413FF92F3E70;
ALTER TABLE tours_countries ADD CONSTRAINT FK_7AA0413F15ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id) ON DELETE CASCADE;
ALTER TABLE tours_countries ADD CONSTRAINT FK_7AA0413FF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE;