<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\orders;
use AppBundle\Form\ordersType;

/**
 * orders controller.
 *
 * @Route("/orders")
 */
class ordersController  extends BaseController
{

    /**
     * Lists all orders entities.
     *
     * @Route("/", name="orders")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:orders')->findAll();
        $tours = [];
        /** @var orders $entity */
        foreach($entities as $entity){
            $tour = $em->getRepository('AppBundle:tour')->find($entity->getTourId());
            $tours[$entity->getId()] = $tour;
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array(
            'entities' => $entities,
            'pagination'  => $pagination,
            'page'        => $page,
            'tours'       => $tours,
        );
    }
    /**
     * Creates a new orders entity.
     *
     * @Route("/", name="orders_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $this->changeCharset();
        $entity = new orders();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $entity->setCreated(new \DateTime('NOW'));
        $result = 0;
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $result = 1;
            //return $this->redirect($this->generateUrl('orders_show', array('id' => $entity->getId())));
        }

        return array(
            //'entity' => $entity,
            //'form'   => $form->createView(),
            'result' => $result
        );
    }

    /**
     * Creates a form to create a orders entity.
     *
     * @param orders $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(orders $entity)
    {
        $form = $this->createForm(new ordersType(), $entity, array(
            'action' => $this->generateUrl('orders_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new orders entity.
     *
     * @Route("/new", name="orders_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->redirectToRoute('orders');
        /*
        $this->changeCharset();
        $entity = new orders();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );

        */
    }

    /**
     * Finds and displays a orders entity.
     *
     * @Route("/{id}", name="orders_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();
        /** @var orders $entity */
        $entity = $em->getRepository('AppBundle:orders')->find($id);
        $tour = $em->getRepository('AppBundle:orders')->find($entity->getTourId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find orders entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
            'tour'        => $tour
        );
    }

    /**
     * Displays a form to edit an existing orders entity.
     *
     * @Route("/{id}/edit", name="orders_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:orders')->find($id);
        $_SESSION['current_orders_page'] = ($page)?$page:1;
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find orders entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }

    /**
    * Creates a form to edit a orders entity.
    *
    * @param orders $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(orders $entity)
    {
        $form = $this->createForm(new ordersType(), $entity, array(
            'action' => $this->generateUrl('orders_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing orders entity.
     *
     * @Route("/{id}", name="orders_update")
     * @Method("PUT")
     * @Template("AppBundle:orders:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find orders entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $page = (isset($_SESSION['current_orders_page']))?$_SESSION['current_orders_page']:1;
        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('orders_edit', array('id' => $id,'page' => $page)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a orders entity.
     *
     * @Route("/{id}", name="orders_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:orders')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find orders entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('orders'));
    }

    /**
     * Creates a form to delete a orders entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('orders_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Удалить'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }


    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $this->changeCharset();
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }
}
