<?php

namespace AppBundle\Controller;

use AppBundle\Entity\gallery;
use AppBundle\Entity\image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\tour;
use AppBundle\Form\tourType;

/**
 * tour controller.
 *
 * @Route("/tour")
 */
class tourController extends BaseController
{

    /**
     * Lists all tour entities.
     *
     * @Route("/", name="tour")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:tour')->findAll();

        $filledEntities = array();
        foreach ($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);
            //$filledEntities[] = $entity;
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );


        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination,
            'page'        => $page,
        );
    }
    /**
     * Creates a new tour entity.
     *
     * @Route("/", name="tour_create")
     * @Method("POST")
     * @Template("AppBundle:tour:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->changeCharset();
        $entity = new tour();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $entity->setCreated(new \DateTime('NOW'));
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('tour'));
            //return $this->redirect($this->generateUrl('tour_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a tour entity.
     *
     * @param tour $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(tour $entity)
    {
        $form = $this->createForm(new tourType(), $entity, array(
            'action' => $this->generateUrl('tour_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new tour entity.
     *
     * @Route("/new", name="tour_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->changeCharset();
        $entity = new tour();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a tour entity.
     *
     * @Route("/{id}", name="tour_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();
        /** @var tour $entity */
        $entity = $em->getRepository('AppBundle:tour')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find apartment entity.');
        }
        $images = [];
        $galleries = $entity->getGalleries();
        /** @var gallery $gallery */
        foreach($galleries as $gallery){
            /** @var image $image */
            foreach($gallery->getImages() as $image){
                $images[] = $image;
            }

        }
        unset($image);
        /** @var image $image */
        foreach($entity->getImages() as $image){
            $images[] = $image;
        }
        $img = '';
        unset($image);
        /** @var image $image */
        foreach($images as $image){
            /** @var string $img */
            $img .= '<img src="/uploads/images/'.$image->getPath().'" width="150" height="125" />';
        }
        $active = ($entity->isActive())?'Активен':'Неактивен';
        $early = ($entity->isEarly())?'Раннее бронирование':'';
        $offers = ($entity->isOffers())?'Специальное предложение':'';
        $burning = ($entity->isBurning())?'Горячее предложение':'';
        $durations = $entity->getDurations();
        $countries = $entity->getCountries();
        $cities = $entity->getCities();
        $services = $entity->getServices();
        $excursions = $entity->getExcursions();
        $managers = $entity->getManagers();
        $groups = $entity->getGroups();

        $entity = $this->fillTranslatedEntity($entity);




        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'image'       => $img,
            'active'      => $active,
            'early'       => $early,
            'offers'      => $offers,
            'burning'     => $burning,
            'durations'   => $durations,
            'countries'   => $countries,
            'cities'      => $cities,
            'services'    => $services,
            'excursions'  => $excursions,
            'managers'    => $managers,
            'groups'      => $groups,
            'page'        => $page,

        );
    }

    /**
     * Displays a form to edit an existing tour entity.
     *
     * @Route("/{id}/edit", name="tour_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();
        $_SESSION['current_tour_page'] = ($page)?$page:1;
        $entity = $em->getRepository('AppBundle:tour')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tour entity.');
        }
        /** @var tour $entity */
        $entity = $this->fillTranslatedEntity($entity);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }

    /**
    * Creates a form to edit a tour entity.
    *
    * @param tour $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(tour $entity)
    {
        $form = $this->createForm(new tourType(), $entity, array(
            'action' => $this->generateUrl('tour_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing tour entity.
     *
     * @Route("/{id}", name="tour_update")
     * @Method("PUT")
     * @Template("AppBundle:tour:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();
        /** @var tour  $entity */
        $entity = $em->getRepository('AppBundle:tour')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tour entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $page = (isset($_SESSION['current_tour_page']))?$_SESSION['current_tour_page']:1;
        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('tour', ['page' => $page]));
            //return $this->redirect($this->generateUrl('tour_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }
    /**
     * Deletes a tour entity.
     *
     * @Route("/{id}", name="tour_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->changeCharset();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:tour')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find tour entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tour'));
    }

    /**
     * Creates a form to delete a tour entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tour_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Удалить'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $this->changeCharset();
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $this->changeCharset();
        $entity = new tour();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-film')
        );

    }
}
