<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\groups;
use AppBundle\Form\groupsType;

/**
 * groups controller.
 *
 * @Route("/groups")
 */
class groupsController extends BaseController
{

    /**
     * Lists all groups entities.
     *
     * @Route("/", name="groups")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:groups')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            15/*limit per page*/
        );


        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination,
            'page'        => $page,
        );
    }
    /**
     * Creates a new groups entity.
     *
     * @Route("/", name="groups_create")
     * @Method("POST")
     * @Template("AppBundle:groups:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->changeCharset();
        $entity = new groups();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('groups'));
            //return $this->redirect($this->generateUrl('groups_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a groups entity.
     *
     * @param groups $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(groups $entity)
    {
        $form = $this->createForm(new groupsType(), $entity, array(
            'action' => $this->generateUrl('groups_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new groups entity.
     *
     * @Route("/new", name="groups_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->changeCharset();
        $entity = new groups();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a groups entity.
     *
     * @Route("/{id}", name="groups_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();
        /** @var groups $entity */
        $entity = $em->getRepository('AppBundle:groups')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find groups entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }

    /**
     * Displays a form to edit an existing groups entity.
     *
     * @Route("/{id}/edit", name="groups_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:groups')->find($id);
        $_SESSION['current_groups_page'] = ($page)?$page:1;
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find groups entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }

    /**
    * Creates a form to edit a groups entity.
    *
    * @param groups $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(groups $entity)
    {
        $form = $this->createForm(new groupsType(), $entity, array(
            'action' => $this->generateUrl('groups_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing groups entity.
     *
     * @Route("/{id}", name="groups_update")
     * @Method("PUT")
     * @Template("AppBundle:groups:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:groups')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find groups entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $page = (isset($_SESSION['current_groups_page']))?$_SESSION['current_groups_page']:1;
        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('groups', ['page' => $page]));
            //return $this->redirect($this->generateUrl('groups_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),

        );
    }
    /**
     * Deletes a groups entity.
     *
     * @Route("/{id}", name="groups_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->changeCharset();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:groups')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find groups entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('groups'));
    }

    /**
     * Creates a form to delete a groups entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('groups_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Удалить'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $this->changeCharset();
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $this->changeCharset();
        $entity = new groups();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-film')
        );

    }

}
