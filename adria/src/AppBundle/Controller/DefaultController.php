<?php

namespace AppBundle\Controller;

use AppBundle\Entity\country;
use AppBundle\Entity\gallery;
use AppBundle\Entity\groups;
use AppBundle\Entity\image;
use AppBundle\Entity\orders;
use AppBundle\Entity\result;
use AppBundle\Entity\service;
use AppBundle\Entity\text;
use AppBundle\Entity\tour;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityRepository as repository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class DefaultController extends BaseController
{


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle\Entity\tour')->findBy(['active' => 1], ['id' => 'DESC'], 35);
        $burning = $em->getRepository('AppBundle\Entity\tour')->findBy(['burning' => 1, 'active' => 1],['id' => 'DESC'], 3);
        $early = $em->getRepository('AppBundle\Entity\tour')->findBy(['early' => 1,'active' => 1],['id' => 'DESC'], 3);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);


        $view = 'AppBundle:Default:index.html.twig';

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            15/*limit per page*/
        );
        $pagination->setTemplate('bootstrap-b.html.twig');
        $path = [];
        $img = [];
        /** @var tour $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        unset($p);
        return $this->render(
            $view,
            [
                'entities'    => $pagination,
                'path'        => $path,
                'burning'     => $burning,
                'img'         => $img,
                'early'       => $early,
                'gallery'     => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider'      => ($slider instanceof gallery)?$slider->getImages():[],


            ]
        );

    }
    /**
     * @Route("/info", name="info")
     */
    public function infoAction()
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'info']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'info']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:info.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );

    }
    /**
     * @Route("/visa", name="visa")
     */
    public function visaAction()
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'visa']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'visa']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render('AppBundle:Default:visa.html.twig', [
            'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
            'slider' => ($slider instanceof gallery)?$slider->getImages():[],
            'text' => $text]);

    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction()
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'contact']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'contact']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:contact.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );

    }
    /**
     * @Route("/avia", name="avia")
     */
    public function aviaAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle\Entity\tour')->findBy(['transport' => 1,'active' => 1], ['id' => 'DESC'], 35);
        $burning = $em->getRepository('AppBundle\Entity\tour')->findBy(['burning' => 1, 'transport' => 1,'active' => 1],['id' => 'DESC'], 3);
        $early = $em->getRepository('AppBundle\Entity\tour')->findBy(['early' => 1, 'transport' => 1,'active' => 1],['id' => 'DESC'], 3);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);

        $view = 'AppBundle:Default:avia.html.twig';

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            7/*limit per page*/
        );
        $pagination->setTemplate('bootstrap-b.html.twig');
        $path = [];
        $img = [];
        /** @var tour $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        unset($p);
        return $this->render(
            $view,
            [
                'entities' => $pagination,
                'path' => $path,
                'img'  => $img,
                'burning' => $burning,
                'early'   => $early,
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],

            ]
        );
    }
    /**
     * @Route("/sea", name="sea")
     */
    public function seaAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle\Entity\tour')->findBy(['transport' => 3,'active' => 1], ['created' => 'DESC'], 35);
        $burning = $em->getRepository('AppBundle\Entity\tour')->findBy(['burning' => 1, 'transport' => 3,'active' => 1],['created' => 'DESC'], 3);
        $early = $em->getRepository('AppBundle\Entity\tour')->findBy(['early' => 1, 'transport' => 3,'active' => 1],['created' => 'DESC'], 3);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);

        $view = 'AppBundle:Default:sea.html.twig';

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            7/*limit per page*/
        );
        $pagination->setTemplate('bootstrap-b.html.twig');
        $path = [];
        $img = [];
        /** @var tour $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        unset($p);
        return $this->render(
            $view,
            [
                'entities' => $pagination,
                'path' => $path,
                'img' => $img,
                'burning' => $burning,
                'early'   => $early,
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],

            ]
        );
    }
    /**
     * @Route("/baltic", name="baltic")
     */
    public function balticAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var groups $group */
        $group = $em->getRepository('AppBundle\Entity\groups')->findOneBy(['title' => 'Baltic']);

        $sql = "SELECT tour_id FROM  `tours_groups` WHERE groups_id=".$group->getId() ;

        $result= $em->getConnection()->query($sql)->fetchAll(PDO::FETCH_NUM);
        $ids = [];
        foreach($result as $res){
            $ids = array_merge($ids,$res);
        }



        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $criteria->where($expr->in('id', $ids));
        $entities = $em->getRepository('AppBundle\Entity\tour')->matching($criteria);
        $arr = [];
        $burning = [];
        $early = [];
        /** @var tour $entity */
        foreach($entities as $entity){
            if($entity->isActive()){
                $arr[] = $entity;
                if($entity->isBurning())
                    $burning[] = $entity;
                if($entity->isEarly())
                    $early[] = $entity;
            }
        }
        $entities = $arr;
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);

        $view = 'AppBundle:Default:baltic.html.twig';

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            7/*limit per page*/
        );
        $pagination->setTemplate('bootstrap-b.html.twig');
        $path = [];
        $img = [];
        /** @var tour $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        unset($p);
        return $this->render(
            $view,
            [
                'entities' => $pagination,
                'path' => $path,
                'img'  => $img,
                'burning' => $burning,
                'early'   => $early,
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],

            ]
        );
    }
    /**
     * @Route("/europe", name="europe")
     */
    public function europeAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle\Entity\tour')->findBy(['transport' => 2,'active' => 1], ['created' => 'DESC'], 35);
        $burning = $em->getRepository('AppBundle\Entity\tour')->findBy(['burning' => 1, 'transport' => 2,'active' => 1],['created' => 'DESC'], 3);
        $early = $em->getRepository('AppBundle\Entity\tour')->findBy(['early' => 1, 'transport' => 2],['created' => 'DESC'], 3);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);

        $view = 'AppBundle:Default:europe.html.twig';

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            7/*limit per page*/
        );
        $pagination->setTemplate('bootstrap-b.html.twig');
        $path = [];
        $img = [];
        /** @var tour $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        unset($p);
        return $this->render(
            $view,
            [
                'entities' => $pagination,
                'path' => $path,
                'img'  => $img,
                'burning' => $burning,
                'early'   => $early,
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],

            ]
        );
    }
    /**
     * @Route("/latvia", name="latvia")
     */
    public function latviaAction()
    {

        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'latvia']);
        /** @var gallery $gallery */
        $latvia = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'latvia']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
            /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        $burning = $em->getRepository('AppBundle\Entity\tour')->findBy(['burning' => 1],['created' => 'DESC'], 3);
        $early = $em->getRepository('AppBundle\Entity\tour')->findBy(['early' => 1],['created' => 'DESC'], 3);
        return $this->render(
            'AppBundle:Default:latvia.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text'    => $text,
                'latvia'  => ($latvia instanceof gallery)?$latvia->getImages():[],
                'burning' => $burning,
                'early'   => $early,
            ]
        );
    }

    /**
     * @Route("/tour/{id}", name="item", requirements={"id" = "\d+"}, defaults={"id" = 1})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tourAction($id)
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var tour $tour */
        $tour = $em->getRepository('AppBundle\Entity\tour')->find($id);
        if(!$tour)
            return $this->redirect('/lv/');
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        $img = '';
        $daysText = '';
        $nightText = '';
        $inclusive = [];
        $exclusive = [];
        $children = [];
        $documents = [];
        $additions = [];
        $order = $this->handleOrder();

        if($tour){

            $images = $tour->getImages();
            $image = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i){
               if($i->getCategory() == 'main'){
                   $image = $i;
                   break;
               }


            }
            $path = ($image)?'/uploads/images/'.$image->getPath():"";
            $img = ($image)?"<img src={$path}  width=600 height=450 class='main-image'>":'';
            $texts = $tour->getTexts();

            /** @var text $txt */
            foreach($texts as $txt){
                if($txt->getTag() &&  $txt->getTag()->getTitle() == 'days')
                    $daysText = $txt->getText();
                if($txt->getTag() &&  $txt->getTag()->getTitle() == 'night')
                    $nightText = $txt->getText();
                if($txt->getTag() &&  $txt->getTag()->getTitle() == 'children')
                    $children[] = $txt;
                if($txt->getTag() &&  $txt->getTag()->getTitle() == 'document')
                    $documents[] = $txt;
                if($txt->getTag() &&  $txt->getTag()->getTitle() == 'additional')
                    $additions[] = $txt;
            }
            /** @var service $service */
            foreach($tour->getServices() as $service){
                if($service->getType()->getCategory() == 'inclusive')
                    $inclusive[] = $service;
                if($service->getType()->getCategory() == 'exclusive')
                    $exclusive[] = $service;
            }

        } else {
            $this->redirect('/lv/');
        }
        return $this->render('AppBundle:Default:tour.html.twig',
            [
                'image'     => $img,
                'gallery'   => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider'    => ($slider instanceof gallery)?$slider->getImages():[],
                'tour'      => $tour,
                'daysText'  => $daysText,
                'nightText' => $nightText,
                'inclusive' => $inclusive,
                'exclusive' => $exclusive,
                'children'  => $children,
                'documents' => $documents,
                'additions' => $additions,
                'order'     => $order,
            ]
        );
    }

    /**
     * @Route("/tour2/{id}", name="item2", requirements={"id" = "\d+"}, defaults={"id" = 1})
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tour2Action($id)
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var tour $tour */
        $tour = $em->getRepository('AppBundle\Entity\tour')->find($id);
        if(!$tour)
            return $this->redirect('/lv/');
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        $img = '';

        $order = $this->handleOrder();

        if($tour){
            $images = $tour->getImages();
            $image = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i){
                if($i->getCategory() == 'main'){
                    $image = $i;
                    break;
                }


            }
            $path = ($image)?'/uploads/images/'.$image->getPath():"";
            $img = ($image)?"<img src={$path}  width=600 height=450 class='main-image'>":'';


        } else {
            $this->redirect('/lv/');
        }
        return $this->render('AppBundle:Default:tour2.html.twig',
            [
                'image'     => $img,
                'gallery'   => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider'    => ($slider instanceof gallery)?$slider->getImages():[],
                'tour'      => $tour,
                'order'     =>$order,

            ]
        );
    }




    /**
     * @Route("/result/{id}", name="result", requirements={"id" = "\d+"}, defaults={"id" = 1})
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resultAction($id)
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var tour $tour */
        $tour = $em->getRepository('AppBundle\Entity\tour')->find($id);
        /** @var result $result */
        $results = $em->getRepository('AppBundle\Entity\result')->findAll();
        $result = null;
        /** @var result $r */
        foreach($results as  $r){
            /** @var tour $tr */
            if($tr = $r->getTour()){
                if($tr->getId() == $id){
                    $result = $r;
                }
            }
        }
        if(!($tour && $result ))
            $this->redirect('/lv/');
        return $this->render('AppBundle:Default:result.html.twig',
            [
                'result'     => $result,
            ]
        );
    }




    /**
     * @Route("/offers", name="offers")
     */
    public function offersAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /*
        /** @var groups $groups *//*
        $groups = $em->getRepository('AppBundle\Entity\groups')->findOneBy(['title' => 'Offers']);

        $sql = "SELECT tour_id FROM  `tours_groups` WHERE groups_id=".$groups->getId();
        $result= $em->getConnection()->query($sql)->fetchAll(PDO::FETCH_NUM);
        $ids = [];
        foreach($result as $res){
            $ids = array_merge($ids,$res);
        }

        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $criteria->where($expr->in('id', $ids));
        $entities = $em->getRepository('AppBundle\Entity\tour')->matching($criteria);
        */
        $entities = $em->getRepository('AppBundle\Entity\tour')->findBy(['offers' => 1,'active' => 1]);
        $arr = [];
        $burning = [];
        $early = [];
        /** @var tour $entity */
        foreach($entities as $entity){
            if($entity->isActive()){
                $arr[] = $entity;
                if($entity->isBurning())
                    $burning[] = $entity;
                if($entity->isEarly())
                    $early[] = $entity;
            }
        }
        $entities = $arr;

        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);

        $view = 'AppBundle:Default:offers.html.twig';

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            7/*limit per page*/
        );
        $pagination->setTemplate('bootstrap-b.html.twig');
        $path = [];
        $img = [];
        /** @var tour $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        unset($p);
        return $this->render(
            $view,
            [
                'entities' => $pagination,
                'path' => $path,
                'img'  => $img,
                'burning' => $burning,
                'early'   => $early,
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],

            ]
        );
    }

    /**
     * @Route("/agreement", name="agreement")
     */
    public function agreementAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'agreement']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'agreement']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:agreement.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );
    }
    /**
     * @Route("/free", name="free")
     */
    public function freeAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'free']);
        $text = $this->fillTranslatedEntity($text);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'free']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:free.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );
    }
    /**
     * @Route("/russia", name="russia")
     */
    public function russiaAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'russia']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'russia']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:russia.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );
    }
    /**
     * @Route("/individual", name="individual")
     */
    public function individualAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'individual']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'individual']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:individual.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );
    }

    /**
     * @Route("/school", name="school")
     */
    public function schoolAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'school']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'school']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:school.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );
    }

    /**
     * @Route("/corp", name="corp")
     */
    public function corpAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'corp']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'corp']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:corp.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );
    }

    /**
     * @Route("/church", name="church")
     */
    public function churchAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'church']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'church']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        return $this->render(
            'AppBundle:Default:church.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'text' => $text
            ]
        );
    }

    /**
     * @Route("/health", name="health")
     */
    public function healthAction()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'health']);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'health']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);
        /** @var gallery $sliderLeft */
        $sliderLeft = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-left-slider']);
        return $this->render(
            'AppBundle:Default:health.html.twig',
            [
                'gallery' => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider' => ($slider instanceof gallery)?$slider->getImages():[],
                'sliderLeft' => ($sliderLeft instanceof gallery)?$sliderLeft->getImages():[],
                'text' => $text
            ]
        );
    }

    public function sliderAction(){
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $tours = $em->getRepository('AppBundle\Entity\tour')->findBy(['slider' => 1]);
        $img = [];
        /** @var tour $p */
        foreach($tours  as $p){
            $images = $p->getImages();
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'top-slider') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        return $this->render(
            'AppBundle::top-slider.html.twig',
            [

                'tours' => $tours,
                'image' => $img,

            ]
        );
    }
    public function fancyAction(){
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $tours = $em->getRepository('AppBundle\Entity\tour')->findBy(['slider' => 1], ['created' => 'DESC'], 15);
        $img = [];
        /** @var tour $p */
        foreach($tours  as $p){
            $images = $p->getImages();
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        return $this->render(
            'AppBundle::slider-left.html.twig',
            [

                'sliders' => $tours,
                'image' => $img,

            ]
        );
    }

    /**
     * @Route("/graph", name="graph")
     */
    public function graphAction(){
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle\Entity\text')->findOneBy(['route' => 'schedule']);

        return $this->render(
            'AppBundle:Default:schedule.html.twig',
            [
                'text' => $text
            ]
        );
    }

    /**
     * @Route("/countries", name="countries")
     */
    public function countriesAction(){
        $this->changeCharset();
        $countries = $this->getCountries();
        return $this->render(
            'AppBundle:Default:countries.html.twig',
            [
                'countries' => $countries
            ]
        );


    }

    /**
     * @Route("/country/{id}", name="country-tours", requirements={"id" = "\d+"}, defaults={"id" = 1})
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function countryAction($id){
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var country $country */
        $country = $em->getRepository('AppBundle\Entity\country')->find($id);

        if(!$country)
            return $this->redirect('/lv/');

        $burning = $em->getRepository('AppBundle\Entity\tour')->findBy(['burning' => 1, 'active' => 1],['id' => 'DESC'], 3);
        $early = $em->getRepository('AppBundle\Entity\tour')->findBy(['early' => 1,'active' => 1],['id' => 'DESC'], 3);
        /** @var gallery $gallery */
        $gallery = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-gallery']);
        /** @var gallery $slider */
        $slider = $em->getRepository('AppBundle\Entity\gallery')->findOneBy(['title' => 'main-slider']);


        $images = $country->getImages();
        $image = null;
        /** @var image $img */
        foreach($images as $img){
            if($img->getCategory() == 'countries-images'){
                $image = $img;
            }
        }
        if(!$image){
            $image = $em->getRepository('AppBundle\Entity\image')->find(20);
        }

        $sql = "SELECT tour_id FROM  `tours_countries`, `tour` WHERE tours_countries.country_id=".$country->getId()." AND tours_countries.tour_id=tour.id AND tour.active=1;"  ;

        $result= $em->getConnection()->query($sql)->fetchAll(PDO::FETCH_NUM);
        $ids = [];
        foreach($result as $res){
            $ids = array_merge($ids,$res);
        }
        $entities = new ArrayCollection();
        foreach($ids as $eid){
            $tour = $em->getRepository('AppBundle\Entity\tour')->find($eid);
            if($tour){
                $entities->add($tour);
            }
        }

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            7/*limit per page*/
        );
        $pagination->setTemplate('bootstrap-b.html.twig');
        $path = [];
        $img = [];
        /** @var tour $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }
            $img[$p->getId()] = image::getDefaultImage($em);
            /** @var image $i */

            foreach($images as $i) {
                if ($i->getCategory() == 'main') {
                    $img[$p->getId()] = $i;
                    break;
                }
            }

        }
        unset($p);


        return $this->render(
            'AppBundle:Default:country.html.twig',
            [
                'entities' => $pagination,
                'path' => $path,
                'img'  => $img,
                'image'    => $image,
                'country'  => $country,
                'burning'     => $burning,
                'early'       => $early,
                'gallery'     => ($gallery instanceof gallery)?$gallery->getImages():[],
                'slider'      => ($slider instanceof gallery)?$slider->getImages():[],
            ]
        );





    }



    private function handleOrder()
    {
        $request = $this->get('request')->request;
        $em = $this->getDoctrine()->getManager();
        $key = parse_url($_SERVER['REQUEST_URI']);
        $key = $key['path'];
        if($request->get('delete-order')){
            if(isset($_COOKIE[$key]) && is_numeric($_COOKIE[$key])){
                $id = $_COOKIE[$key];
                $order = $em->getRepository('AppBundle\Entity\orders')->find($id);
                $em->remove($order);
                $em->flush();
                $order = $em->getRepository('AppBundle\Entity\orders')->find($id);
                if(!($order instanceof orders)){
                    setcookie($key, "");
                    $this->redirect($_SERVER['REQUEST_URI']);
                }
            }
        }
        if($request->get('feedback-submitted')){
            if(isset($_COOKIE[$key])){
                $id = $_COOKIE[$key];
                $order = $em->getRepository('AppBundle\Entity\orders')->find($id);
            }else{
                $order = new orders();
            }
            return $this->fillOrder($order, $request, $key, $em);


        }else{
            $date = null;
            if(isset($_COOKIE[$key]) && is_numeric($_COOKIE[$key])){
                $id = $_COOKIE[$key];
                $order = $em->getRepository('AppBundle\Entity\orders')->find($id);
                if(!($order instanceof orders)){
                    setcookie($key, "");
                }else{
                    $date = $order->getDate();
                }
            }

            return (isset($_COOKIE[$key]))?$date:null;

        }

    }

    private function fillOrder(orders $order,  $request, $key, EntityManager $em){
        if($order instanceof orders){
            $order->setName(trim($request->get('feedback-name')));
            $order->setEmail(trim($request->get('feedback-email')));
            $order->setPhone(trim($request->get('feedback-phone')));
            $order->setText(trim($request->get('feedback-text')));
            $order->setTourId((int) trim($request->get('feedback-tour')));
            $order->setDate(new \DateTime(trim($request->get('feedback-date'))) );
            $order->setCreated(new \DateTime('NOW'));
            $created = $order->getCreated()->getTimestamp();
            $dt = $order->getDate()->getTimestamp();
            $diff = (is_numeric($dt))?abs($dt-$created):0;
            $time =  time();
            $exp = $time + $diff;

            $em->persist($order);
            $em->flush();
            $id = $order->getId();
            if($id){
                setcookie($key, $id, $exp);
                return $order->getDate();
            }else{
                setcookie($key, "");
                return null;
            }
        }else{
            setcookie($key, "");
            return null;
        }

    }

    private function getCountries()
    {
        $this->changeCharset();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle\Entity\tour')->findBy(['active' => 1], ['id' => 'DESC'], 350);
        $countriesIds =[];
        /** @var tour $entity */
        foreach($entities as $entity){
            $cs = $entity->getCountries();
            /** @var country $c */
            foreach($cs as $c){
                $id = $c->getId();
                if(!in_array($id, $countriesIds))
                    array_push($countriesIds, $id);
            }
        }
        $countries = [];
        $sorted = [];
        foreach($countriesIds as $sid){
            $country = $em->getRepository('AppBundle\Entity\country')->find($sid);
            if($country instanceof country)
            $sorted[$sid] = $country->getTitle();
        }
        $sortedOld = array_flip($sorted);
        sort($sorted, SORT_STRING);

        $countriesIds = [];
        foreach($sorted as $value){
            $countriesIds[] = $sortedOld[$value];
        }
        foreach($countriesIds as $id){
            /** @var country $country */
            $country = $em->getRepository('AppBundle\Entity\country')->find($id);
            if($country instanceof country)
                array_push($countries, $country);
        }

        return $countries;
    }

    /**
     * @Route("/ksu", name="ksu")
     */
    public function ksuAction(){
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle\Entity\tour')->findBy([], null, 15);
        $serializer = $this->container->get('jms_serializer');
        $res = $serializer->serialize($entities, 'json');

        return new JsonResponse(json_decode($res));

    }


}
