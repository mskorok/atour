<?php

namespace AppBundle\Controller;

use AppBundle\Entity\gallery;
use AppBundle\Entity\image;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\duration;
use AppBundle\Form\durationType;

/**
 * duration controller.
 *
 * @Route("/duration")
 */
class durationController extends BaseController
{

    /**
     * Lists all duration entities.
     *
     * @Route("/", name="duration")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->changeCharset();

        $page = $this->get('request')->query->get('page');

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:duration')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            15/*limit per page*/
        );


        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination,
            'page'        => $page,
        );
    }
    /**
     * Creates a new duration entity.
     *
     * @Route("/", name="duration_create")
     * @Method("POST")
     * @Template("AppBundle:duration:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->changeCharset();
        $entity = new duration();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('duration'));
            //return $this->redirect($this->generateUrl('duration_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a duration entity.
     *
     * @param duration $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(duration $entity)
    {

        $form = $this->createForm(new durationType(), $entity, array(
            'action' => $this->generateUrl('duration_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new duration entity.
     *
     * @Route("/new", name="duration_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->changeCharset();
        $entity = new duration();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a duration entity.
     *
     * @Route("/{id}", name="duration_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');

        $em = $this->getDoctrine()->getManager();
        /** @var duration $entity */
        $entity = $em->getRepository('AppBundle:duration')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find apartment entity.');
        }
        $images = [];
        $galleries = $entity->getGalleries();
        /** @var gallery $gallery */
        foreach($galleries as $gallery){
            /** @var image $image */
            foreach($gallery->getImages() as $image){
                $images[] = $image;
            }

        }
        unset($image);
        /** @var image $image */
        foreach($entity->getImages() as $image){
            $images[] = $image;
        }
        $img = '';
        unset($image);
        /** @var image $image */
        foreach($images as $image){
            /** @var string $img */
            $img .= '<img src="/uploads/images/'.$image->getPath().'" width="150" height="125" />';
        }

        $entity = $this->fillTranslatedEntity($entity);




        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'image'       => $img,
            'page'        => $page
        );
    }

    /**
     * Displays a form to edit an existing duration entity.
     *
     * @Route("/{id}/edit", name="duration_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:duration')->find($id);
        $_SESSION['current_duration_page'] = ($page)?$page:1;
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find duration entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page' => $page
        );
    }

    /**
    * Creates a form to edit a duration entity.
    *
    * @param duration $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(duration $entity)
    {
        $form = $this->createForm(new durationType(), $entity, array(
            'action' => $this->generateUrl('duration_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing duration entity.
     *
     * @Route("/{id}", name="duration_update")
     * @Method("PUT")
     * @Template("AppBundle:duration:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:duration')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find duration entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $page = (isset($_SESSION['current_duration_page']))?$_SESSION['current_duration_page']:1;
        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('duration', ['page' => $page]));
            //return $this->redirect($this->generateUrl('duration_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a duration entity.
     *
     * @Route("/{id}", name="duration_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->changeCharset();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:duration')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find duration entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('duration'));
    }

    /**
     * Creates a form to delete a duration entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('duration_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Удалить'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $this->changeCharset();
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $this->changeCharset();
        $entity = new duration();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-film')
        );

    }

}
