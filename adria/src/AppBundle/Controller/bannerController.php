<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\banner;
use AppBundle\Form\bannerType;

/**
 * banner controller.
 *
 * @Route("/banner")
 */
class bannerController extends BaseController
{

    /**
     * Lists all banner entities.
     *
     * @Route("/", name="banner")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:banner')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            15/*limit per page*/
        );


        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination,
            'page'        => $page,
        );
    }
    /**
     * Creates a new banner entity.
     *
     * @Route("/", name="banner_create")
     * @Method("POST")
     * @Template("AppBundle:banner:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->changeCharset();
        $entity = new banner();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            //return $this->redirect($this->generateUrl('banner_show', array('id' => $entity->getId())));
            return $this->redirect($this->generateUrl('banner'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a banner entity.
     *
     * @param banner $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(banner $entity)
    {
        $form = $this->createForm(new bannerType(), $entity, array(
            'action' => $this->generateUrl('banner_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new banner entity.
     *
     * @Route("/new", name="banner_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->changeCharset();
        $entity = new banner();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a banner entity.
     *
     * @Route("/{id}", name="banner_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();
        /** @var banner $entity */
        $entity = $em->getRepository('AppBundle:banner')->find($id);

        $image= $entity->getImage();
        $img = '<img src="/uploads/images/thumbnail/'.$image->getPath().'" width="150" height="125" />';

        $entity = $this->fillTranslatedEntity($entity);




        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'image'       => $img,
            'page'        => $page,
        );
    }

    /**
     * Displays a form to edit an existing banner entity.
     *
     * @Route("/{id}/edit", name="banner_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();
        $_SESSION['current_banner_page'] = ($page)?$page:1;
        $entity = $em->getRepository('AppBundle:banner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find banner entity.');
        }
        /** @var banner $entity */
        $entity = $this->fillTranslatedEntity($entity);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }

    /**
    * Creates a form to edit a banner entity.
    *
    * @param banner $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(banner $entity)
    {
        $form = $this->createForm(new bannerType(), $entity, array(
            'action' => $this->generateUrl('banner_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing banner entity.
     *
     * @Route("/{id}", name="banner_update")
     * @Method("PUT")
     * @Template("AppBundle:banner:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:banner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find banner entity.');
        }
        /** @var banner $entity */
        $entity = $this->fillTranslatedEntity($entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $page = (isset($_SESSION['current_banner_page']))?$_SESSION['current_banner_page']:1;
        if ($editForm->isValid()) {
            $em->flush();

            //return $this->redirect($this->generateUrl('banner_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('banner', ['page' => $page]));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }
    /**
     * Deletes a banner entity.
     *
     * @Route("/{id}", name="banner_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->changeCharset();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:banner')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find banner entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('banner'));
    }

    /**
     * Creates a form to delete a banner entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('banner_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Удалить'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {

        $this->changeCharset();
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {

        $this->changeCharset();
        $entity = new banner();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-film')
        );

    }
}
