<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\currency;
use AppBundle\Form\currencyType;

/**
 * currency controller.
 *
 * @Route("/currency")
 */
class currencyController extends BaseController
{

    /**
     * Lists all currency entities.
     *
     * @Route("/", name="currency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:currency')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            15/*limit per page*/
        );


        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination,
            'page'        => $page,
        );
    }
    /**
     * Creates a new currency entity.
     *
     * @Route("/", name="currency_create")
     * @Method("POST")
     * @Template("AppBundle:currency:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->changeCharset();

        $entity = new currency();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('currency'));
            //return $this->redirect($this->generateUrl('currency_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a currency entity.
     *
     * @param currency $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(currency $entity)
    {
        $form = $this->createForm(new currencyType(), $entity, array(
            'action' => $this->generateUrl('currency_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new currency entity.
     *
     * @Route("/new", name="currency_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->changeCharset();
        $entity = new currency();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a currency entity.
     *
     * @Route("/{id}", name="currency_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:currency')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find currency entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }

    /**
     * Displays a form to edit an existing currency entity.
     *
     * @Route("/{id}/edit", name="currency_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:currency')->find($id);
        $_SESSION['current_currency_page'] = ($page)?$page:1;
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find currency entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page,
        );
    }

    /**
    * Creates a form to edit a currency entity.
    *
    * @param currency $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(currency $entity)
    {
        $form = $this->createForm(new currencyType(), $entity, array(
            'action' => $this->generateUrl('currency_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing currency entity.
     *
     * @Route("/{id}", name="currency_update")
     * @Method("PUT")
     * @Template("AppBundle:currency:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:currency')->find($id);
        $page = (isset($_SESSION['current_currency_page']))?$_SESSION['current_currency_page']:1;
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find currency entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('currency', ['page' => $page]));
            //return $this->redirect($this->generateUrl('currency_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a currency entity.
     *
     * @Route("/{id}", name="currency_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:currency')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find currency entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currency'));
    }

    /**
     * Creates a form to delete a currency entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currency_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Удалить'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $this->changeCharset();
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $this->changeCharset();
        $entity = new currency();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-film')
        );

    }

}
