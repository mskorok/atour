<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\result;
use AppBundle\Form\resultType;

/**
 * result controller.
 *
 * @Route("/result")
 */
class resultController extends BaseController
{

    /**
     * Lists all result entities.
     *
     * @Route("/", name="result")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:result')->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            15/*limit per page*/
        );

        return array(
            'entities' => $entities,
            'pagination'  => $pagination,
            'page'        => $page,
        );
    }
    /**
     * Creates a new result entity.
     *
     * @Route("/", name="result_create")
     * @Method("POST")
     * @Template("AppBundle:result:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->changeCharset();
        $entity = new result();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('result_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a result entity.
     *
     * @param result $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(result $entity)
    {
        $form = $this->createForm(new resultType(), $entity, array(
            'action' => $this->generateUrl('result_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new result entity.
     *
     * @Route("/new", name="result_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->changeCharset();
        $entity = new result();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a result entity.
     *
     * @Route("/{id}", name="result_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->changeCharset();
        $page = $this->get('request')->query->get('page');

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:result')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find result entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'page'        => $page
        );
    }

    /**
     * Displays a form to edit an existing result entity.
     *
     * @Route("/{id}/edit", name="result_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

        $this->changeCharset();
        $page = $this->get('request')->query->get('page');

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:result')->find($id);
        $_SESSION['current_result_page'] = ($page)?$page:1;
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find result entity.');
        }
        $_SESSION['current_result_page'] = ($page)?$page:1;
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'page'        => $page
        );
    }

    /**
    * Creates a form to edit a result entity.
    *
    * @param result $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(result $entity)
    {
        $form = $this->createForm(new resultType(), $entity, array(
            'action' => $this->generateUrl('result_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing result entity.
     *
     * @Route("/{id}", name="result_update")
     * @Method("PUT")
     * @Template("AppBundle:result:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {

        $this->changeCharset();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:result')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find result entity.');
        }
        $page = (isset($_SESSION['current_result_page']))?$_SESSION['current_result_page']:1;
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('result_edit', array('id' => $id,'page' => $page)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a result entity.
     *
     * @Route("/{id}", name="result_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        $this->changeCharset();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:result')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find result entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('result'));
    }

    /**
     * Creates a form to delete a result entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('result_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Удалить'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $this->changeCharset();
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $this->changeCharset();
        $entity = new result();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'AppBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-result')
        );

    }



}
