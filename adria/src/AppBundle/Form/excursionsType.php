<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class excursionsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['required' => true])
            ->add('description')
            ->add('text')
            ->add('type', null, ['required' => true])
            ->add('countries', null, ['required' => true])
            ->add('cities', null, ['required' => true])
            ->add('price', null, ['required' => true])
            ->add('images', null, ['required' => true])
            ->add('galleries', null, ['required' => true])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\excursions'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_excursions';
    }
}
