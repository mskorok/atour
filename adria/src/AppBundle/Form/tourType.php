<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class tourType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['required' => true])
            ->add('description')
            ->add('text')
            ->add('weight')
            ->add('active')
            ->add('burning')
            ->add('early')
            ->add('offers')
            ->add('slider')
            ->add('creator', null, ['required' => true])
            ->add('countries')
            ->add('cities')
            ->add('transport', null, ['required' => true])
            ->add('price', null, ['required' => true])
            ->add('durations')
            ->add('dates')
            ->add('services')
            ->add('excursions')
            ->add('images')
            ->add('galleries')
            ->add('managers')
            ->add('groups')
            ->add('texts')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\tour'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_tour';
    }
}
