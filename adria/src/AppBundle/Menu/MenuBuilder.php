<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 22.05.2014
 * Time: 8:15
 */

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
class MenuBuilder extends ContainerAware{

    /**
     *
     *
     * <ul class="bottom-nav-block">
     *     <li class="inner-li-bottom-nav-left">
     *         <a href="{{ path('actor') }}" class="bottom-nav-link" >{% trans %}Actor{% endtrans %}</a>
     *     </li>
     *     <li class="inner-li-bottom-nav">
     *         <a href="{{ path('author') }}" class="bottom-nav-link">{% trans %}Author{% endtrans %}</a>
     *     </li>
     *     <li class="inner-li-bottom-nav-right">
     *         <a href="{{ path('tag') }}" class="bottom-nav-link">{% trans %}Tag{% endtrans %}</a>
     *     </li>
     * </ul>
     *
     *
     *
     * @param FactoryInterface $factory
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function bottomAdminMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');


        //$admin     = $this->container->get('translator')->trans('администратор');
        $banner      = $this->container->get('translator')->trans('флаг');
        $category    = $this->container->get('translator')->trans('категория');
        //$chi         = $this->container->get('translator')->trans('дети');
        $city        = $this->container->get('translator')->trans('город');
        $country     = $this->container->get('translator')->trans('страна');
        $currency    = $this->container->get('translator')->trans('валюта');
        $date        = $this->container->get('translator')->trans('дата');
        //$documents   = $this->container->get('translator')->trans('документы');
        $duration    = $this->container->get('translator')->trans('дни тура');
        $excursions  = $this->container->get('translator')->trans('экскурсии');
        $gallery     = $this->container->get('translator')->trans('галерея');
        $groups      = $this->container->get('translator')->trans('группы');
        $image       = $this->container->get('translator')->trans('изображение');
        $price       = $this->container->get('translator')->trans('цена');
        $service     = $this->container->get('translator')->trans('услуги');
        $result      = $this->container->get('translator')->trans('результат');
        $tag         = $this->container->get('translator')->trans('тэг');
        $text        = $this->container->get('translator')->trans('текст');
        $tour        = $this->container->get('translator')->trans('тур');
        $transport   = $this->container->get('translator')->trans('транспорт');


        //$menu->addChild($admin, array('route' => 'admin'));
        $menu->addChild($banner, array('route' => 'banner'));
        $menu->addChild($category, array('route' => 'category'));
        //$menu->addChild($chi, array('route' => 'children'));
        $menu->addChild($city, array('route' => 'city'));
        $menu->addChild($country, array('route' => 'country'));
        $menu->addChild($currency, array('route' => 'currency'));
        $menu->addChild($date, array('route' => 'date'));
        //$menu->addChild($documents, array('route' => 'documents'));
        $menu->addChild($duration, array('route' => 'duration'));
        $menu->addChild($excursions, array('route' => 'excursions'));
        $menu->addChild($gallery, array('route' => 'gallery'));
        $menu->addChild($groups, array('route' => 'groups'));
        $menu->addChild($image, array('route' => 'image'));
        $menu->addChild($price, array('route' => 'price'));
        $menu->addChild($service, array('route' => 'service'));
        $menu->addChild($result, array('route' => 'result'));
        $menu->addChild($tag, array('route' => 'tag'));
        $menu->addChild($text, array('route' => 'text'));
        $menu->addChild($tour, array('route' => 'tour'));
        $menu->addChild($transport, array('route' => 'transport'));

        // ... add more children
        //$menu['Actor']->setLinkAttribute('class', 'external-link');
        //$menu['Actor']->setLabelAttribute('class', 'no-link-span');
        $menu->setChildrenAttribute('class', 'bottom-nav-block');
        $menu[$banner]->setAttribute('class', 'inner-li-bottom-nav-left')->setLinkAttribute('class', 'bottom-nav-link');
        //$menu[$admin]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$category]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        //$menu[$chi]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$city]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$country]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$currency]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$date]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        //$menu[$documents]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$duration]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$excursions]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$gallery]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$groups]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$image]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$price]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$service]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$result]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$tag]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$text]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$tour]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$transport]->setAttribute('class', 'inner-li-bottom-nav-right')->setLinkAttribute('class', 'bottom-nav-link');

        return $menu;
    }


    /**
     *
     * <ul class="top-nav-block">
    <li class="inner-li-top-nav-left">
    <a href="{{ path('film') }}" class="top-nav-link" >{% trans %}Film{% endtrans %}</a>
    </li>
    <li class="inner-li-top-nav">
    <a href="{{ path('book') }}" class="top-nav-link">{% trans %}Book{% endtrans %}</a>
    </li>
    <li class="inner-li-top-nav-right">
    <a href="{{ path('sonata_admin_dashboard') }}" class="top-nav-link">Sonata</a>
    </li>
    </ul>
     *
     * @param FactoryInterface $factory
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function topAdminMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');



        $banner      = $this->container->get('translator')->trans('флаг');
        $category    = $this->container->get('translator')->trans('категория');
        $city        = $this->container->get('translator')->trans('город');
        $country     = $this->container->get('translator')->trans('страна');
        $currency    = $this->container->get('translator')->trans('валюта');
        $date        = $this->container->get('translator')->trans('дата');
        $duration    = $this->container->get('translator')->trans('дни тура');
        $excursions  = $this->container->get('translator')->trans('экскурсии');
        $gallery     = $this->container->get('translator')->trans('галерея');
        $groups      = $this->container->get('translator')->trans('группы');
        $image       = $this->container->get('translator')->trans('изображение');
        $price       = $this->container->get('translator')->trans('цена');
        $order       = $this->container->get('translator')->trans('заказ');
        $schedule    = $this->container->get('translator')->trans('расписание');
        $service     = $this->container->get('translator')->trans('услуги');
        $result      = $this->container->get('translator')->trans('результат');
        $tag         = $this->container->get('translator')->trans('тэг');
        $text        = $this->container->get('translator')->trans('текст');
        $tour        = $this->container->get('translator')->trans('тур');
        $transport   = $this->container->get('translator')->trans('транспорт');




        $menu->addChild($banner, array('route' => 'banner'));
        $menu->addChild($category, array('route' => 'category'));
        $menu->addChild($city, array('route' => 'city'));
        $menu->addChild($country, array('route' => 'country'));
        $menu->addChild($currency, array('route' => 'currency'));
        $menu->addChild($date, array('route' => 'date'));
        $menu->addChild($duration, array('route' => 'duration'));
        $menu->addChild($excursions, array('route' => 'excursions'));
        $menu->addChild($gallery, array('route' => 'gallery'));
        $menu->addChild($groups, array('route' => 'groups'));
        $menu->addChild($image, array('route' => 'image'));
        $menu->addChild($price, array('route' => 'price'));
        $menu->addChild($order, array('route' => 'orders'));
        $menu->addChild($schedule, array('route' => 'schedule'));
        $menu->addChild($service, array('route' => 'service'));
        $menu->addChild($result, array('route' => 'result'));
        $menu->addChild($tag, array('route' => 'tag'));
        $menu->addChild($text, array('route' => 'text'));
        $menu->addChild($tour, array('route' => 'tour'));
        $menu->addChild($transport, array('route' => 'transport'));
        // ... add more children
        //$menu['Actor']->setLinkAttribute('class', 'external-link');
        //$menu['Actor']->setLabelAttribute('class', 'no-link-span');
        $menu->setChildrenAttribute('class', 'top-nav-block');
        $menu[$banner]->setAttribute('class', 'inner-li-top-nav-left')->setLinkAttribute('class', 'top-nav-link');
        $menu[$category]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$city]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$country]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$currency]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$date]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$duration]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$excursions]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$gallery]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$groups]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$image]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$price]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$order]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$schedule]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$service]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$result]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$tag]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$text]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$tour]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$transport]->setAttribute('class', 'inner-li-top-nav-right')->setLinkAttribute('class', 'top-nav-link');



        return $menu;
    }
} 