<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 22.05.2014
 * Time: 8:15
 */

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
class MenuBuilderMain extends ContainerAware
{

    /**
     *
     * <ul class="top-nav-block">
    <li class="inner-li-top-nav-left">
    <a href="{{ path('film') }}" class="top-nav-link" >{% trans %}Film{% endtrans %}</a>
    </li>
    <li class="inner-li-top-nav">
    <a href="{{ path('book') }}" class="top-nav-link">{% trans %}Book{% endtrans %}</a>
    </li>
    <li class="inner-li-top-nav-right">
    <a href="{{ path('sonata_admin_dashboard') }}" class="top-nav-link">Sonata</a>
    </li>
    </ul>
     *
     * @param FactoryInterface $factory
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function topMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');


        $home       = $this->container->get('translator')->trans('На<br><br>главную');
        $avia      = $this->container->get('translator')->trans('Авиа<br><br>туры');
        $sea          = $this->container->get('translator')->trans('Морские <br><br>круизы');
        $europe            = $this->container->get('translator')->trans('Путешествия<br><br>по Европе');
        $baltic       = $this->container->get('translator')->trans('Экскурсии<br><br>по Балтии');
        $latvia            = $this->container->get('translator')->trans('Прием туристов<br><br>в Латвии');
        $offers           = $this->container->get('translator')->trans('Специальные<br><br>предложения');
        $info           = $this->container->get('translator')->trans('Информация<br><br>для туристов');
        $contacts           = $this->container->get('translator')->trans('<br>Контакты');


        $menu->addChild($home, array('route' => 'homepage'));
        $menu->addChild($avia, array('route' => 'avia'));
        $menu->addChild($sea, array('route' => 'sea'));
        $menu->addChild($europe, array('route' => 'europe'));
        $menu->addChild($baltic, array('route' => 'baltic'));
        $menu->addChild($latvia, array('route' => 'latvia'));
        $menu->addChild($offers, array('route' => 'offers'));
        $menu->addChild($info, array('route' => 'latvia'));
        $menu->addChild($contacts, array('route' => 'offers'));
        // ... add more children
        //$menu['Actor']->setLinkAttribute('class', 'external-link');
        //$menu['Actor']->setLabelAttribute('class', 'no-link-span');
        $menu->setChildrenAttribute('class', 'menu');
        $menu[$home]->setAttribute('class', 'b-m left-b-m');
        $menu[$avia]->setAttribute('class', 'b-m');
        $menu[$sea]->setAttribute('class', 'b-m');
        $menu[$europe]->setAttribute('class', 'b-m');
        $menu[$baltic]->setAttribute('class', 'b-m');
        $menu[$latvia]->setAttribute('class', 'b-m');
        $menu[$offers]->setAttribute('class', 'b-m');
        $menu[$info]->setAttribute('class', 'b-m');
        $menu[$contacts]->setAttribute('class', 'b-m right-b-m');

        return $menu;
    }
} 