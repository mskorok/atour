<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * banner
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\bannerRepository")
 */
class banner extends entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var country
     *
     *
     * @ORM\OneToOne(targetEntity="country", mappedBy="banner")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     *
     *
     */
    protected $country;

    /**
     * @var mixed
     *
     * @ORM\OneToOne(targetEntity="image")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    protected $image;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param country $country
     * @return banner
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set image
     *
     * @param mixed $image
     * @return banner
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    public function __toString(){
        return $this->country->getTitle();
    }
}
