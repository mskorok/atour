<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * admin
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\adminRepository")
 */
class admin extends BaseUser implements Translatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="surname", type="string", length=255,  nullable=true)
     */
    protected $surname;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="position", type="string", length=255,  nullable=true)
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255,  nullable=true)
     */
    protected $phone;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime",  nullable=true)
     */
    protected $created;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="image")
     * @ORM\JoinTable(name="user_images",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $images;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="gallery")
     * @ORM\JoinTable(name="user_galleries",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="gallery_id", referencedColumnName="id")}
     *      )
     */
    protected $galleries;
    /**
     * @var string
     *
     *  @Gedmo\Locale
     */
    protected $locale;

    public function __construct()
    {
        parent::__construct();

        $this->roles = array(static::ROLE_DEFAULT);
        $this->images = new ArrayCollection();
        $this->galleries = new ArrayCollection();

    }

    public function __toString(){
        return $this->username;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set surname
     *
     * @param string $surname
     * @return admin
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return admin
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return admin
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }



    /**
     * Set created
     *
     * @param \DateTime $created
     * @return admin
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return ArrayCollection
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * @param ArrayCollection $galleries
     */
    public function setGalleries($galleries)
    {
        $this->galleries = $galleries;
    }

    /**
     * @return mixed
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale) {
        $this->locale = $locale;
    }
}
