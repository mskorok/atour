<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Tour
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\tourRepository")
 */
class Tour extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime",  nullable=true)
     */
    protected $created;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="duration")
     * @ORM\JoinTable(name="tours_durations",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="duration_id", referencedColumnName="id")}
     *      )
     */
    protected $durations;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="admin")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     */
    protected $creator;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="country")
     * @ORM\JoinTable(name="tours_countries",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country_id", referencedColumnName="id")}
     *      )
     */
    protected $countries;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="city")
     * @ORM\JoinTable(name="tours_cities",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id")}
     *      )
     */
    protected $cities;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="transport")
     * @ORM\JoinColumn(name="transport_id", referencedColumnName="id")
     */
    protected $transport;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="string", length=255,  nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="text", type="text",  nullable=true)
     */
    protected $text;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="text")
     * @ORM\JoinTable(name="tours_texts",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="text_id", referencedColumnName="id")}
     *      )
     */
    protected $texts;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="price")
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id")
     */
    protected $price;

    /**
     * @var ArrayCollection
     *
     *
     * @ORM\ManyToMany(targetEntity="service")
     * @ORM\JoinTable(name="tours_services",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id")}
     *      )
     *
     *
     */
    protected $services;

    /**
     * @var ArrayCollection
     *
     *
     * @ORM\ManyToMany(targetEntity="excursions")
     * @ORM\JoinTable(name="tours_excursions",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")}
     *      )
     *
     *
     */
    protected $excursions;


    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="image")
     * @ORM\JoinTable(name="tours_images",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")}
     *      )
     */
    protected $images;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="date")
     * @ORM\JoinTable(name="tours_dates",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="date_id", referencedColumnName="id")}
     *      )
     */
    protected $dates;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="gallery")
     * @ORM\JoinTable(name="tours_galleries",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="gallery_id", referencedColumnName="id")}
     *      )
     */
    protected $galleries;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="admin")
     * @ORM\JoinTable(name="tours_managers",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="manager_id", referencedColumnName="id")}
     *      )
     */
    protected $managers;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="groups")
     * @ORM\JoinTable(name="tours_groups",
     *      joinColumns={@ORM\JoinColumn(name="tour_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="groups_id", referencedColumnName="id")}
     *      )
     */
    protected $groups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean",  nullable=true)
     */
    protected $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="early", type="boolean",  nullable=true)
     */
    protected $early;

    /**
     * @var boolean
     *
     * @ORM\Column(name="burning", type="boolean",  nullable=true)
     */
    protected $burning;

    /**
     * @var boolean
     *
     * @ORM\Column(name="offers", type="boolean",  nullable=true)
     */
    protected $offers;
    /**
     * @var boolean
     *
     * @ORM\Column(name="slider", type="boolean",  nullable=true)
     */
    protected $slider;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer",  nullable=true)
     *
     */
    protected $weight;




    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->creator = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->countries = new ArrayCollection();
        $this->durations = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->excursions = new ArrayCollection();
        $this->managers = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->texts = new ArrayCollection();
        $this->dates = new ArrayCollection();
    }


    public function __toString()
    {
        return $this->getTitle();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return tour
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return ArrayCollection
     */
    public function getDurations()
    {
        return $this->durations;
    }

    /**
     * @param ArrayCollection $durations
     */
    public function setDurations($durations)
    {
        $this->durations = $durations;
    }

    /**
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return ArrayCollection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param ArrayCollection $countries
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param ArrayCollection $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

    /**
     * @return mixed
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param mixed $transport
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return ArrayCollection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param ArrayCollection $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

    /**
     * @return ArrayCollection
     */
    public function getExcursions()
    {
        return $this->excursions;
    }

    /**
     * @param ArrayCollection $excursions
     */
    public function setExcursions($excursions)
    {
        $this->excursions = $excursions;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return ArrayCollection
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * @param ArrayCollection $galleries
     */
    public function setGalleries($galleries)
    {
        $this->galleries = $galleries;
    }

    /**
     * @return ArrayCollection
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * @param ArrayCollection $managers
     */
    public function setManagers($managers)
    {
        $this->managers = $managers;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param ArrayCollection $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return boolean
     */
    public function isBurning()
    {
        return $this->burning;
    }

    /**
     * @param boolean $burning
     */
    public function setBurning($burning)
    {
        $this->burning = $burning;
    }

    /**
     * @return boolean
     */
    public function isEarly()
    {
        return $this->early;
    }

    /**
     * @param boolean $early
     */
    public function setEarly($early)
    {
        $this->early = $early;
    }

    /**
     * @return boolean
     */
    public function isOffers()
    {
        return $this->offers;
    }

    /**
     * @param boolean $offers
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
    }

    /**
     * @return ArrayCollection
     */
    public function getTexts()
    {
        return $this->texts;
    }

    /**
     * @param ArrayCollection $texts
     */
    public function setTexts($texts)
    {
        $this->texts = $texts;
    }

    /**
     * @return ArrayCollection
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @param ArrayCollection $dates
     */
    public function setDates($dates)
    {
        $this->dates = $dates;
    }

    /**
     * @return boolean
     */
    public function isSlider()
    {
        return $this->slider;
    }

    /**
     * @param boolean $slider
     */
    public function setSlider($slider)
    {
        $this->slider = $slider;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
}
