<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * service
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\serviceRepository")
 */
class service extends entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="tag")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="tag")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id",  nullable=true)
     */
    protected $category;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="tag")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id",  nullable=true)
     */
    protected $subcategory;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="string", length=255,  nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="text", type="text",  nullable=true)
     */
    protected $text;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="price")
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id",  nullable=true)
     */
    protected $price;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="image")
     * @ORM\JoinTable(name="services_images",
     *      joinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")}
     *      )
     */
    protected $images;



    public function __construct() {

        $this->images = new ArrayCollection();



    }

    public function __toString(){
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }

    /**
     * @param mixed $subcategory
     */
    public function setSubcategory($subcategory)
    {
        $this->subcategory = $subcategory;
    }



    /**
     * Set title
     *
     * @param string $title
     * @return service
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return service
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return service
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return ArrayCollection
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param ArrayCollection $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }



}
