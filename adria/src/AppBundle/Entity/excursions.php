<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * excursions
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\excursionsRepository")
 */
class excursions extends entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="tag")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id",  nullable=true)
     *
     */
    protected $type;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="country")
     * @ORM\JoinTable(name="excursions_countries",
     *      joinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country_id", referencedColumnName="id")}
     *      )
     */
    protected $countries;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="city")
     * @ORM\JoinTable(name="excursions_cities",
     *      joinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id")}
     *      )
     */
    protected $cities;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="string", length=255,  nullable=true)
     */
    protected $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="text")
     * @ORM\JoinTable(name="excursions_texts",
     *      joinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="text_id", referencedColumnName="id")}
     *      )
     */
    protected $texts;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="text", type="text",  nullable=true)
     */
    protected $text;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="price")
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id")
     */
    protected $price;




    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="image")
     * @ORM\JoinTable(name="excursions_images",
     *      joinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")}
     *      )
     */
    protected $images;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="gallery")
     * @ORM\JoinTable(name="excursions_galleries",
     *      joinColumns={@ORM\JoinColumn(name="excursion_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="gallery_id", referencedColumnName="id")}
     *      )
     */
    protected $galleries;


    public function __construct() {

        $this->images = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->countries = new ArrayCollection();
        $this->texts = new ArrayCollection();

    }

    public function __toString(){
        return $this->getTitle();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return excursions
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return excursions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return excursions
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return ArrayCollection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param ArrayCollection $countries
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param ArrayCollection $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return ArrayCollection
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * @param ArrayCollection $galleries
     */
    public function setGalleries($galleries)
    {
        $this->galleries = $galleries;
    }

    /**
     * @return ArrayCollection
     */
    public function getTexts()
    {
        return $this->texts;
    }

    /**
     * @param ArrayCollection $texts
     */
    public function setTexts($texts)
    {
        $this->texts = $texts;
    }



}
